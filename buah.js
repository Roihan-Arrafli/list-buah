const listBuah = [
    {
        namabuah: "Jeruk",
        rasa: "Asam"
    },
    {
        namabuah: "Pisang",
        rasa: "Manis"
    },
    {
        namabuah: "Apel",
        rasa: "Manis"
    },
    {
        namabuah: "Sirsak",
        rasa: "Asam"
    },
    {
        namabuah: "Buah Cokelat",
        rasa: "Pahit"
    },
    {
        namabuah: "Matoa",
        rasa: "Manis"
    }
];

for(let i = 0; i<listBuah.length; i++){
    console.log(listBuah[i]["namabuah"], 
    ("berasa " + listBuah[i]["rasa"]));
}

console.log("-cari-")
function cariBuah(namabuah) {
    for(let i = 0; i <listBuah.length; i++){
        if (listBuah[i]["namabuah"] == namabuah) {
            console.log(listBuah[i]["namabuah"], 
            ("berasa " + 
            listBuah[i]["rasa"]));
        }
    }
}
cariBuah("Buah Cokelat");

console.log("-filter-");
function filterBuah(rasa) {
    for(let i = 0; i < listBuah.length; i++){
        if (listBuah[i]["rasa"] == rasa) {
            console.log(listBuah[i]["namabuah"]);
        }
    }
}
filterBuah("Asam")